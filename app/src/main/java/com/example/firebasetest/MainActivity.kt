package com.example.firebasetest

import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging

class MainActivity : AppCompatActivity() {

    val TAG = "Token Firebase"
    private val receiver by lazy {
        MyNotificationReceiver{
            Toast.makeText(baseContext, "Notification received", Toast.LENGTH_LONG).show()
        }

    }

    override fun onStart() {
        super.onStart()
        registerReceiver(receiver, IntentFilter().apply {
            addAction(ACTION_UPDATE_UI)
        })
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(receiver)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirebaseMessaging.getInstance().isAutoInitEnabled = true

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d(TAG, token.toString())
                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            })
    }
}