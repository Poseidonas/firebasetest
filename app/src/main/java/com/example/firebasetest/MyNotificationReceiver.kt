package com.example.firebasetest

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent


const val ACTION_UPDATE_UI="UPDATEUI"


class MyNotificationReceiver(private inline val synchronieUI: ()-> Unit) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        if(intent?.action== ACTION_UPDATE_UI){
             synchronieUI()

        }
    }
}
